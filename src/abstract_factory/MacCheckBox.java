package abstract_factory;

public class MacCheckBox implements Checkbox {
    @Override
    public void paint() {
        System.out.println("Mac check box");
    }
}
