package abstract_factory;

public class TestDesignPatterns {
    public static void main(String[] args) throws Exception {
        init("Windows");
        OSAbstractFactory osAbstractFactory = new OSAbstractFactory(factory);
        osAbstractFactory.buttonPaint();
        osAbstractFactory.checkBoxPaint();
    }

    private static GUIFactory factory;

    public static void init(String type) throws Exception {
        if (type.equals("Windows")) {
            factory = new WinFactory();
        } else if (type.equals("Mac")) {
            factory = new MacFactory();
        } else {
            throw new Exception("Error! Unknown operating system.");
        }
    }
}
