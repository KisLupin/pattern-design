package abstract_factory;

public class OSAbstractFactory {
    private final Button button;
    private final Checkbox checkbox;

    public OSAbstractFactory(GUIFactory guiFactory) {
        this.button = guiFactory.createButton();
        this.checkbox = guiFactory.createCheckbox();
    }

    public void buttonPaint() {
        this.button.paint();
    }

    public void checkBoxPaint() {
        this.checkbox.paint();
    }
}
