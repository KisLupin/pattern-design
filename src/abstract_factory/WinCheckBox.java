package abstract_factory;

public class WinCheckBox implements Checkbox {
    @Override
    public void paint() {
        System.out.println("Win Check box");
    }
}
